import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Supplier} from '../../../../../models/supplier.model';

@Component({
  selector: 'app-edit-warehouse-dialog',
  templateUrl: './edit-supplier-dialog.component.html',
  styleUrls: ['./edit-supplier-dialog.component.scss']
})
export class EditSupplierDialogComponent implements OnInit {
  supplier: Supplier;

  constructor(
    public dialogRef: MatDialogRef<EditSupplierDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    if (this.data.supplier) {
      this.supplier = this.data?.supplier;
    } else {
      this.supplier = {};
    }
  }
}

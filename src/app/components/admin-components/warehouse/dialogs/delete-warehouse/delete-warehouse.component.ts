import {Component, Inject, OnInit} from '@angular/core';
import {Supplier} from '../../../../../models/supplier.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-delete-warehouse',
  templateUrl: './delete-warehouse.component.html',
  styleUrls: ['./delete-warehouse.component.scss']
})
export class DeleteWarehouseComponent implements OnInit {
  warehouse: Supplier;

  constructor(
    public dialogRef: MatDialogRef<DeleteWarehouseComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit(): void {
    this.warehouse = this.data.warehouse;
  }

}

import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Warehouse} from '../../../../../models/warehouse.model';
import {SupplierService} from '../../../supplier/supplier.service';
import {Supplier} from '../../../../../models/supplier.model';

@Component({
  selector: 'app-edit-warehouse-dialog',
  templateUrl: './edit-warehouse-dialog.component.html',
  styleUrls: ['./edit-warehouse-dialog.component.scss']
})
export class EditWarehouseDialogComponent implements OnInit {
  warehouse: Warehouse;
  suppliers: Supplier[];

  constructor(
    public dialogRef: MatDialogRef<EditWarehouseDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private supplierService: SupplierService) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    if (this.data.warehouse) {
      this.warehouse = this.data.warehouse;
    }
    this.getSuppliers();
  }

  getSuppliers(): void {
    this.supplierService.getSuppliers(0, 1, true).subscribe(res => {
      this.suppliers = res.body;
    });
  }

  compareObjects(o1: any, o2: any): boolean {
    return o1.name === o2.name && o1.id === o2.id;
  }
}

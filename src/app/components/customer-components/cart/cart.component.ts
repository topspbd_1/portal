import {Component, OnInit} from '@angular/core';
import {StoreStateService} from '../../../common-services/store-state.service';
import {State} from '../../../models/state.model';
import {MatDialog} from '@angular/material/dialog';
import {AddressChooserComponent} from '../address/dialog/address-chooser/address-chooser.component';
import {OrderService} from './order.service';
import {Order} from '../../../models/order.model';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Router} from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  cartState: State;
  totalPrice: any = 0;
  isOrderPlaced: boolean;

  constructor(private stateService: StoreStateService,
              private dialog: MatDialog,
              private router: Router,
              private orderService: OrderService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.cartState = this.stateService.getCartState();
    this.cartState.products
      .map(value => value.price)
      .forEach(value => this.totalPrice += value);
  }

  // Handler pentru a plasa o comanda
  async placeOrder(): Promise<void> {
    const dialogRef = this.dialog.open(AddressChooserComponent);

    const dialogResult = await dialogRef.afterClosed().toPromise();

    const order = new Order();

    order.products = this.cartState.products;
    order.orderDetails = dialogResult.orderDetails;
    order.deliveryAddress = `${dialogResult.deliveryAddress?.address}, ${dialogResult.deliveryAddress?.postalCode}, ${dialogResult.deliveryAddress?.city}`;

    try {
      const placedOrder = await this.orderService.placeOrder(order).toPromise();
      this.snackBar.open('Comanda a fost plasata cu succes', 'OK', {
        duration: 2000,
        verticalPosition: 'top',
        horizontalPosition: 'end',
        panelClass: 'snackbar-success'
      });

      this.stateService.resetState();
      this.router.navigate(['customer/profile']);
    } catch (e) {
      this.snackBar.open('O eroare a aparut', 'OK', {
        duration: 2000,
        verticalPosition: 'top',
        horizontalPosition: 'end',
        panelClass: 'snackbar-fail'
      });
    }
  }
}

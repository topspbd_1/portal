export class Supplier {
  id?: string;
  name?: string;
  postalCode?: string;
  address?: string;

  constructor(init?: Partial<Supplier>) {
    Object.assign(this, init);
  }
}

export enum SupplierDetailsType {
  PRODUCT = 'PRODUCT',
  WAREHOUSE = 'WAREHOUSE'
}

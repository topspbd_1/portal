export class Address {
    id?: string;
    address?: string;
    city?: string;
    postalCode?: string;

  constructor(init?: Partial<Address>) {
    Object.assign(this, init);
  }
}


